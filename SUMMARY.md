# Table of contents

* [Home](README.md)

## Contributing

* [Introduction](contributing/introduction.md)
* [Website](contributing/website.md)

## API

* [Introduction](api/introduction.md)
* [v1](api/v1/README.md)
  * [Beta Access](api/v1/beta-access.md)
  * [Credits](api/v1/credits.md)
  * [Language File](api/v1/language-file-1.md)
  * [Presences](api/v1/presences.md)
  * [Users](api/v1/users.md)
* [v2](api/v2/README.md)
  * [Credits](api/v2/credits.md)
  * [Language File](api/v2/language-file.md)
  * [Versions](api/v2/version.md)
  * [Presences](api/v2/presences.md)
* [Ping](api/ping.md)

## Presence Development

* [Introduction](presence-development/introduction.md)
* [Coding](presence-development/coding/README.md)
  * [Presence Class](presence-development/coding/presence-class.md)
  * [Metadata File](presence-development/coding/metadata.json.md)
  * [TypeScript Configuration](presence-development/coding/tsconfig.json.md)

