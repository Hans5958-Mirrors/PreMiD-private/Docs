# Website

{% hint style="info" %}
We use VueJS as our main framework. Visit [their homepage](https://vuejs.org/) for further information.
{% endhint %}

### Branches

| master | stable |
| :--- | :--- |
| https://beta.premid.app | https://premid.app |

### Starting a local server

Open a terminal and type `npm run serve`. Wait for it to finish building.  
Your local server is now running at `localhost:8080`

