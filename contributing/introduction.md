# Introduction

### Basic knowledge\*

* [JavaScript](https://www.javascript.com/) &gt; [TypeScript](https://www.typescriptlang.org/)
* html5

Additional:

| Component | Software/Language |
| :--- | :--- |
| [API](../api/introduction.md) | [TypeScript](https://www.typescriptlang.org/) |
| Discord bot | [TypeScript](https://www.typescriptlang.org/) |
| PreMiD \(application\) | [ElectronJS](https://electronjs.org/), [TypeScript](https://www.typescriptlang.org/) |
| PreMiD \(extension\) | [VueJS](https://vuejs.org/) \(Tabs\*\*\) |
| Website | CSS, [VueJS](https://vuejs.org/) |

\*Required for all components, \*\*E.g. [installed.html](https://i.imgur.com/4wUWMON.png)

{% hint style="info" %}
A source code editor is also required. We recommend [Visual Studio Code](https://code.visualstudio.com/).
{% endhint %}

### Installing the requirements

1. Install [Git](https://git-scm.com/)
2. Install [Node](https://nodejs.org/en/) \(comes with [npm](https://www.npmjs.com/)\)
3. Install [TypeScript](https://www.typescriptlang.org/index.html#download-links) \(open a terminal and type `npm install -g typescript`\)

### Cloning the project

1. Open a terminal and type `git clone URL` **replace URL with repository link you want to contribute to** e.g.  `git clone` [`https://github.com/PreMiD/PreMiD`](https://github.com/PreMiD/PreMiD)\`\`
2. Choose a folder of your choice
3. Open it in you code editor

### Installing dependencies

{% hint style="warning" %}
Be sure you've installed [npm ](https://www.npmjs.com/)\(Node Package Manager\) first. It automatically installed itself if you've [Node](https://nodejs.org/en/) installed.
{% endhint %}

Open a terminal in your repository and type `npm i`.  
To update the dependencies type `npm update`.

{% hint style="danger" %}
Be aware that updating the dependencies could brake a lot, or not - testing is required!
{% endhint %}

### Coding your vision

Please keep the structure. We don't want to disorganize our project. Chaotic files may not be accepted.

### Uploading your dream

Just open a Pull Request on one of our [GitHub repositories](https://github.com/PreMiD/) you want to contribute to.

