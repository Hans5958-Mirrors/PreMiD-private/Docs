# Presences

{% api-method method="get" host="https://api.premid.app" path="/v2/presences/:presenceName" %}
{% api-method-summary %}
Get Presences
{% endapi-method-summary %}

{% api-method-description %}
This endpoint allows you to get free cakes.
{% endapi-method-description %}

{% api-method-spec %}
{% api-method-request %}
{% api-method-path-parameters %}
{% api-method-parameter name="presenceName" type="string" %}
Name of presence to get
{% endapi-method-parameter %}
{% endapi-method-path-parameters %}
{% endapi-method-request %}

{% api-method-response %}
{% api-method-response-example httpCode=200 %}
{% api-method-response-example-description %}
Cake successfully retrieved.
{% endapi-method-response-example-description %}

```javascript
// No presenceName passed
[
  {
    "name": "YouTube",
    "metadata": {
      "author": {
        "name": "Timeraa",
        "id": "223238938716798978"
      },
      "service": "YouTube",
      "description": "Enjoy the videos and music you love, upload original content, and share it all with friends, family, and the world on YouTube.",
      "url": "www.youtube.com",
      "version": "2.0",
      "logo": "https://i.imgur.com/C9ZEmge.png",
      "thumbnail": "https://i.imgur.com/3QfIc5v.jpg",
      "color": "#E40813",
      "tags": [
        "video",
        "media"
      ]
    },
    "url": "https://raw.githubusercontent.com/PreMiD/Presences/master/YouTube/dist/"
  },
  …
]

// presenceName passed
{
  "name": "YouTube",
  "metadata": {
    "author": {
      "name": "Timeraa",
      "id": "223238938716798978"
    },
    "service": "YouTube",
    "description": "Enjoy the videos and music you love, upload original content, and share it all with friends, family, and the world on YouTube.",
    "url": "www.youtube.com",
    "version": "2.0",
    "logo": "https://i.imgur.com/C9ZEmge.png",
    "thumbnail": "https://i.imgur.com/3QfIc5v.jpg",
    "color": "#E40813",
    "tags": [
      "video",
      "media"
    ]
  },
  "url": "https://raw.githubusercontent.com/PreMiD/Presences/master/YouTube/dist/"
}
```
{% endapi-method-response-example %}
{% endapi-method-response %}
{% endapi-method-spec %}
{% endapi-method %}



