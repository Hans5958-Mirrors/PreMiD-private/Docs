# Language File

{% api-method method="get" host="https://api.premid.app" path="/v2/langFile/list" %}
{% api-method-summary %}
Language List
{% endapi-method-summary %}

{% api-method-description %}
This endpoint allows you to get the available languages on Transifex.
{% endapi-method-description %}

{% api-method-spec %}
{% api-method-request %}

{% api-method-response %}
{% api-method-response-example httpCode=200 %}
{% api-method-response-example-description %}
Request successfully retrieved.
{% endapi-method-response-example-description %}

```javascript
[
    "de",
    "en",
    …
]
```
{% endapi-method-response-example %}
{% endapi-method-response %}
{% endapi-method-spec %}
{% endapi-method %}

{% api-method method="get" host="https://api.premid.app" path="/v2/langFile/:project/:lang" %}
{% api-method-summary %}
Language File
{% endapi-method-summary %}

{% api-method-description %}
This endpoint allows you to get the translations from Transifex.
{% endapi-method-description %}

{% api-method-spec %}
{% api-method-request %}
{% api-method-path-parameters %}
{% api-method-parameter name="project" type="string" required=true %}
Transifex project \("extension" or "website"\)
{% endapi-method-parameter %}

{% api-method-parameter name="lang" type="string" required=true %}
i18n language code
{% endapi-method-parameter %}
{% endapi-method-path-parameters %}
{% endapi-method-request %}

{% api-method-response %}
{% api-method-response-example httpCode=200 %}
{% api-method-response-example-description %}
Request successfully retrieved.
{% endapi-method-response-example-description %}

```text
This will return the strings of the specific language per project.
```
{% endapi-method-response-example %}
{% endapi-method-response %}
{% endapi-method-spec %}
{% endapi-method %}

{% hint style="info" %}
Our Localization-API is based on chrome.i18n. Visit [its website](https://developer.chrome.com/apps/i18n) for further information.
{% endhint %}

