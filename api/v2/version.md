# Versions

{% api-method method="get" host="https://api.premid.app" path="/v2/versions" %}
{% api-method-summary %}
Versions
{% endapi-method-summary %}

{% api-method-description %}
This endpoint allows you to get the versions of all our products.
{% endapi-method-description %}

{% api-method-spec %}
{% api-method-request %}

{% api-method-response %}
{% api-method-response-example httpCode=200 %}
{% api-method-response-example-description %}
Request successfully retrieved.
{% endapi-method-response-example-description %}

```javascript
{
    "api": "2.0", // version of our API servers
    "app": "2.0", // latest version of our app
    "extension": "2.0" // latest version of our extension
    "bot": "2.0" // latest version of our Discord bot
}
```
{% endapi-method-response-example %}
{% endapi-method-response %}
{% endapi-method-spec %}
{% endapi-method %}

