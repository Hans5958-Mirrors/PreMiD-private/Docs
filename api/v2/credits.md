# Credits

{% api-method method="get" host="https://api.premid.app" path="/v2/credits/:userId" %}
{% api-method-summary %}
Credits
{% endapi-method-summary %}

{% api-method-description %}
This endpoint allows you to get the user metadata of the PreMiD staff.
{% endapi-method-description %}

{% api-method-spec %}
{% api-method-request %}
{% api-method-path-parameters %}
{% api-method-parameter name="userId" type="string" %}
UserId of user to get
{% endapi-method-parameter %}
{% endapi-method-path-parameters %}
{% endapi-method-request %}

{% api-method-response %}
{% api-method-response-example httpCode=200 %}
{% api-method-response-example-description %}
Request successfully retrieved.
{% endapi-method-response-example-description %}

```javascript
// No userId passed
[{
	"userId": "259407123782434816",
	"name": "Fruxh",
	"tag": "3282",
	"avatar": "https://cdn.discordapp.com/avatars/259407123782434816/a_e58b5662a4f69071942a0ed158b64fee.gif",
	"role": "Developer",
	"roleColor": "#71cdff",
	"rolePosition": 31
}…]

// UserId passed
{
	"userId": "259407123782434816",
	"name": "Fruxh",
	"tag": "3282",
	"avatar": "https://cdn.discordapp.com/avatars/259407123782434816/a_e58b5662a4f69071942a0ed158b64fee.gif",
	"role": "Developer",
	"roleColor": "#71cdff",
	"rolePosition": 31
}
```
{% endapi-method-response-example %}

{% api-method-response-example httpCode=404 %}
{% api-method-response-example-description %}
Could not find a request matching this query.
{% endapi-method-response-example-description %}

```javascript
{
    "message": "Not Found"
}
```
{% endapi-method-response-example %}
{% endapi-method-response %}
{% endapi-method-spec %}
{% endapi-method %}

{% hint style="info" %}
Our Credits-API is based on DiscordJS. Visit [their website](https://discord.js.org/) for further information.
{% endhint %}

