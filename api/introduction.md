# Introduction

{% code-tabs %}
{% code-tabs-item title="Base URL" %}
```http
https://api.premid.app
```
{% endcode-tabs-item %}
{% endcode-tabs %}

### API Versioning

{% hint style="danger" %}
Some API and Gateway versions are now deprecated and are labeled as discontinued in the table below for posterity.
{% endhint %}

PreMiD exposes different versions of our API. You can specify version by including it in the request path like `https://api.premid.app/v{version_number}`. Omitting the version number from the route will route requests to the current default version \(marked below accordingly\).

| Version | Status | Default |
| :--- | :--- | :--- |
| 2 | Available |  |
| 1 | Deprecated | ✔ |

### Encryption <a id="encryption"></a>

All HTTP-layer services and protocols \(e.g. http\) within the PreMiD API use TLS 1.2.

