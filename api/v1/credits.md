# Credits

{% api-method method="get" host="https://api.premid.app" path="/credits" %}
{% api-method-summary %}
Credits
{% endapi-method-summary %}

{% api-method-description %}
This endpoint allows you to get the user metadata of the PreMiD staff.
{% endapi-method-description %}

{% api-method-spec %}
{% api-method-request %}
{% api-method-path-parameters %}
{% api-method-parameter name="" type="string" required=false %}
No parameters available
{% endapi-method-parameter %}
{% endapi-method-path-parameters %}
{% endapi-method-request %}

{% api-method-response %}
{% api-method-response-example httpCode=200 %}
{% api-method-response-example-description %}
Request successfully retrieved.
{% endapi-method-response-example-description %}

```javascript
[{
	"name": "Fruxh", // Discord username
	"tag": "3282", // Discord tag
	"avatar": "https://cdn.discordapp.com/avatars/259407123782434816/a_e58b5662a4f69071942a0ed158b64fee.gif", // Discord avatar URL
	"role": "Developer", // highest role
	"roleColor": "#71cdff", // hex color of role
	"rolePosition": 31, // position of role
	"userID": "259407123782434816", // Discord user id
	"patronColor": "#fff", // donate to get a special role
	"position": 31, // position in the Discord bot
}]
```
{% endapi-method-response-example %}

{% api-method-response-example httpCode=404 %}
{% api-method-response-example-description %}
Could not find a request matching this path.
{% endapi-method-response-example-description %}

```javascript
{
    "message": "Not Found"
}
```
{% endapi-method-response-example %}
{% endapi-method-response %}
{% endapi-method-spec %}
{% endapi-method %}

{% hint style="info" %}
Our Credits-API is based on DiscordJS. Visit [their website](https://discord.js.org/) for further information.
{% endhint %}

