# Beta Access

{% api-method method="get" host="https://api.premid.app" path="/betaAccess/:userid" %}
{% api-method-summary %}
Beta Access
{% endapi-method-summary %}

{% api-method-description %}
This endpoint allows you to the beta status of a user.
{% endapi-method-description %}

{% api-method-spec %}
{% api-method-request %}
{% api-method-path-parameters %}
{% api-method-parameter name="userid" type="number" required=true %}
Discord user ID
{% endapi-method-parameter %}
{% endapi-method-path-parameters %}
{% endapi-method-request %}

{% api-method-response %}
{% api-method-response-example httpCode=200 %}
{% api-method-response-example-description %}
Request successfully retrieved.
{% endapi-method-response-example-description %}

```javascript
{
    "access": true // or false, depends if the user has the BETA acces role
}
```
{% endapi-method-response-example %}

{% api-method-response-example httpCode=404 %}
{% api-method-response-example-description %}
Could not find a request matching this query.
{% endapi-method-response-example-description %}

```javascript
{
    "message": "Not Found"
}
```
{% endapi-method-response-example %}

{% api-method-response-example httpCode=410 %}
{% api-method-response-example-description %}
Could not find an User ID.
{% endapi-method-response-example-description %}

```javascript
{
    "error": 410,
    "message": "No user id provided."
}
```
{% endapi-method-response-example %}
{% endapi-method-response %}
{% endapi-method-spec %}
{% endapi-method %}

