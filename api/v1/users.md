# Users

{% api-method method="get" host="https://api.premid.app" path="/users" %}
{% api-method-summary %}
Users
{% endapi-method-summary %}

{% api-method-description %}
This endpoint allows you to get the usercount of PreMiD.
{% endapi-method-description %}

{% api-method-spec %}
{% api-method-request %}
{% api-method-path-parameters %}
{% api-method-parameter name="" type="string" %}
No parameters available
{% endapi-method-parameter %}
{% endapi-method-path-parameters %}
{% endapi-method-request %}

{% api-method-response %}
{% api-method-response-example httpCode=200 %}
{% api-method-response-example-description %}
Request successfully retrieved.
{% endapi-method-response-example-description %}

```javascript
{
    "chrome": 11232
}
```
{% endapi-method-response-example %}

{% api-method-response-example httpCode=404 %}
{% api-method-response-example-description %}
Could not find a request matching this query.
{% endapi-method-response-example-description %}

```javascript
{
    "message": "Not Found"
}
```
{% endapi-method-response-example %}
{% endapi-method-response %}
{% endapi-method-spec %}
{% endapi-method %}

