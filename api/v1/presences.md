# Presences

{% api-method method="get" host="https://api.premid.app" path="/presences" %}
{% api-method-summary %}
Presences
{% endapi-method-summary %}

{% api-method-description %}
This endpoint allows you to get all available presences on our /Presences repository.
{% endapi-method-description %}

{% api-method-spec %}
{% api-method-request %}
{% api-method-path-parameters %}
{% api-method-parameter name="presenceName" type="string" %}
To get a specific Presence
{% endapi-method-parameter %}
{% endapi-method-path-parameters %}
{% endapi-method-request %}

{% api-method-response %}
{% api-method-response-example httpCode=200 %}
{% api-method-response-example-description %}
Request successfully retrieved.
{% endapi-method-response-example-description %}

```javascript
[
  {
    "name": "YouTube"
    "url": "https://raw.githubusercontent.com/PreMiD/Presences/master/YouTube/" // GitHub URL
  }
]
```
{% endapi-method-response-example %}

{% api-method-response-example httpCode=404 %}
{% api-method-response-example-description %}
Could not find a request matching this path.
{% endapi-method-response-example-description %}

```javascript
{
    "error": 404,
    "message":"Not Found"
}
```
{% endapi-method-response-example %}

{% api-method-response-example httpCode=406 %}
{% api-method-response-example-description %}
Could not find a specific Presence.
{% endapi-method-response-example-description %}

```javascript
{
    "error": 406,
    "message": "Presence not found."
}
```
{% endapi-method-response-example %}
{% endapi-method-response %}
{% endapi-method-spec %}
{% endapi-method %}

