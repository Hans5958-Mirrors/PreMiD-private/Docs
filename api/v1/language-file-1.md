# Language File

{% api-method method="get" host="https://api.premid.app" path="/langFile/:lang" %}
{% api-method-summary %}
Language File
{% endapi-method-summary %}

{% api-method-description %}
This endpoint allows you to get the translations for our extension from Transifex.
{% endapi-method-description %}

{% api-method-spec %}
{% api-method-request %}
{% api-method-path-parameters %}
{% api-method-parameter name="lang" type="string" required=true %}
i18n language code
{% endapi-method-parameter %}
{% endapi-method-path-parameters %}
{% endapi-method-request %}

{% api-method-response %}
{% api-method-response-example httpCode=200 %}
{% api-method-response-example-description %}
Request successfully retrieved.
{% endapi-method-response-example-description %}

```text
This will return the strings of the specific language.
```
{% endapi-method-response-example %}

{% api-method-response-example httpCode=404 %}
{% api-method-response-example-description %}
Could not find a request matching this path.
{% endapi-method-response-example-description %}

```javascript
{
    "message":"Not Found."
}
```
{% endapi-method-response-example %}
{% endapi-method-response %}
{% endapi-method-spec %}
{% endapi-method %}

{% hint style="info" %}
Our Localization-API is based on chrome.i18n. Visit [its website](https://developer.chrome.com/apps/i18n) for further information.
{% endhint %}

