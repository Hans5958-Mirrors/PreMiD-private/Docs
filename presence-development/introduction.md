# Introduction

{% hint style="info" %}
All presences are now stored here: [https://github.com/PreMiD/Presences](https://github.com/PreMiD/Presences)
{% endhint %}

Version `2.x` introduces the [presence store](https://premid.app/store). Users now have the ability to manually add and remove their favourite presences through the user interface of the [website](https://premid.app/).

### Coding Language

All presences are now coded with [TypeScript](https://www.typescriptlang.org/).

### Presence Versioning

{% hint style="danger" %}
Some versions are now non-functioning, and are labeled as discontinued in the table below for posterity. Trying to use these versions will fail.
{% endhint %}

| Version | Status | Default |
| :--- | :--- | :--- |
| 3 | Beta | ✔ |
| 2 | Available | ✘ |
| 1 | Discontinued | ✘ |

