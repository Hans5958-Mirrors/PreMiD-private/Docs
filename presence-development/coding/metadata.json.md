# Metadata File

If you want to publish a presence to the store and load it via the extension, you should create the `metadata.json` file in your `presence.js` folder.

The example of that file can be found below.

{% code-tabs %}
{% code-tabs-item title="metadata.json" %}
```javascript
{
  "author": {
    "name": "USER",
    "id": "ID"
  },
  "service": "SERVICE",
  "description": "DESCRIPTION",
  "url": "URL",
  "version": "VERSION",
  "logo": "URL",
  "thumbnail": "URL",
  "color": "#45A8FC",
  "tags": ["twitch", "video"],
  "iframe": false
}
```
{% endcode-tabs-item %}

{% code-tabs-item title="example" %}
```javascript
{
  "author": {
    "name": "Fruxh",
    "id": "259407123782434816"
  },
  "service": "Twitch",
  "description": "We are a global community of millions who come together each day to create their own entertainment.",
  "url": "www.twitch.tv",
  "version": "3.0",
  "logo": "https://images-eu.ssl-images-amazon.com/images/I/312EjBsxqzL.png",
  "thumbnail": "https://i.imgur.com/0SFrVOJ.jpg",
  "color": "#6441A5",
  "tags": ["stream", "media"]
}
```
{% endcode-tabs-item %}
{% endcode-tabs %}

## Understanding the metadata.json

That example looks really strange, huh? Don't worry, its not that hard to understand what each variable is for.

<table>
  <thead>
    <tr>
      <th style="text-align:left">Variable</th>
      <th style="text-align:left">Description</th>
      <th style="text-align:left">Type</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="text-align:left"><b>author</b>
      </td>
      <td style="text-align:left">Should contain Object with <code>name</code> and <code>id</code> of the presence
        developer. User <code>id</code> can be copied from Discord by enabling developer
        mode and right-clicking on your profile.</td>
      <td style="text-align:left"><code>Object</code>
      </td>
    </tr>
    <tr>
      <td style="text-align:left"><b>service</b>
      </td>
      <td style="text-align:left">The title of the service that this presence supports.</td>
      <td style="text-align:left"><code>String</code>
      </td>
    </tr>
    <tr>
      <td style="text-align:left"><b>description</b>
      </td>
      <td style="text-align:left">Small description of the presence, you can use description of the service
        if you are out of ideas.</td>
      <td style="text-align:left"><code>String</code>
      </td>
    </tr>
    <tr>
      <td style="text-align:left"><b>url</b>
      </td>
      <td style="text-align:left">
        <p>URL of the service.</p>
        <p><b>Example:</b>  <code>https://vk.com/</code>
        </p>
        <p><b>This url must match the url of the website as it will be used to detect wherever or not this is the website to inject the script to.</b>
        </p>
      </td>
      <td style="text-align:left"><code>String, Array&lt;String&gt;</code>
      </td>
    </tr>
    <tr>
      <td style="text-align:left"><b>version</b>
      </td>
      <td style="text-align:left">Version of your presence.</td>
      <td style="text-align:left"><code>String</code>
      </td>
    </tr>
    <tr>
      <td style="text-align:left"><b>logo</b>
      </td>
      <td style="text-align:left">Link to service&apos;s logotype.</td>
      <td style="text-align:left"><code>String</code>
      </td>
    </tr>
    <tr>
      <td style="text-align:left"><b>thumbnail</b>
      </td>
      <td style="text-align:left">Link to your presence thumbnail.</td>
      <td style="text-align:left"><code>String</code>
      </td>
    </tr>
    <tr>
      <td style="text-align:left"><b>color</b>
      </td>
      <td style="text-align:left"><code>#HEX</code> value. We recommend to use a primary color of the service
        that your presence supports.</td>
      <td style="text-align:left"><code>String</code>
      </td>
    </tr>
    <tr>
      <td style="text-align:left"><b>tags</b>
      </td>
      <td style="text-align:left">Array with tags, they will help users to search your presence on the website.</td>
      <td
      style="text-align:left"><code>Array</code>
        </td>
    </tr>
    <tr>
      <td style="text-align:left"><b>iframe</b>
      </td>
      <td style="text-align:left">Defines whether <code>iFrames</code> are used</td>
      <td style="text-align:left"><code>Boolean</code>
      </td>
    </tr>
  </tbody>
</table>