---
description: The main class for every PreMiD presence.
---

# Presence Class

## Introduction

The `Presence` class is very useful as it has basic methods that we need for creating a presence.

 When you create a class you must specify `clientId` property.

```typescript
let presence = new Presence({
    clientId: "514271496134389561" // Example clientId
});
```

There are two properties available for `Presence` class.

#### `clientId`

`clientId` property must be provided to make your presence work, because it uses your application id to display its logo and assets. 

You can get it on your [application's page](https://discordapp.com/developers/applications).

#### `mediaKeys`

This property tells our app to register the keybindings for media keys and allows us to use `MediaKeys` event for the `Presence` class.

This property is not required, but if you want to enable media keys you should set it to `true`

```typescript
let presence = new Presence({
    clientId: "514271496134389561",
    mediaKeys: true // Allows users to use media keys
});
```

## Methods

### `setActivity(presenceData, Boolean)`

Sets your profile activity according to provided data.

First parameter requires an `presenceData` interface to get all information that you want to display in your profile.

Second parameter defines when presence is playing something or not. Always use `true` if you provide timestamps in `presenceData`

### `clearActivity()`

Clears your current activity, the keybinds and the tray title.

### `setTrayTitle(String)`

Sets the tray title on the Menubar.

{% hint style="warning" %}
This method works only on Mac OS.
{% endhint %}

### `getStrings(Object)`

Allows you to get translated strings from extension.  
You must provide `Object` with keys being the key for string, `keyValue` is the string value.

```typescript
// Returns `Playing` and `Paused` strings
// from extension.
strings = await presence.getStrings({
    play: "presence.playback.playing",
    pause: "presence.playback.paused"
});
```

### `getPageVariable(String)`

Returns a variable from the website if it exists.

```typescript
var pageVar = getPageVariable('.pageVar');
console.log(pageVar); // This will log the "Variable content"
```

## `presenceData` Interface

The `presenceData` interface is recommended to use when you're using the `setActivity()` method.

This interface has following variables, all of them are optional.

<table>
  <thead>
    <tr>
      <th style="text-align:left">Variable</th>
      <th style="text-align:left">Description</th>
      <th style="text-align:left">Type</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="text-align:left">state</td>
      <td style="text-align:left">The first line in your presence, usually used as header.</td>
      <td style="text-align:left"><code>String</code>
      </td>
    </tr>
    <tr>
      <td style="text-align:left">details</td>
      <td style="text-align:left">Second line in your presence.</td>
      <td style="text-align:left"><code>String</code>
      </td>
    </tr>
    <tr>
      <td style="text-align:left">startTimestamp</td>
      <td style="text-align:left">
        <p>Defines the current time.</p>
        <p>
          <br />Used if you want to display how much <code>hours:minutes:seconds</code> left.
          <br
          />You must convert your time to <code>timestamp</code> or you will get a wrong
          countdown.</p>
      </td>
      <td style="text-align:left"><code>Number</code>
      </td>
    </tr>
    <tr>
      <td style="text-align:left">endTimestamp</td>
      <td style="text-align:left">
        <p>Defines the full duration.</p>
        <p>
          <br />Used if you want to display how much <code>hours:minutes:seconds</code> left.
          <br
          />You must convert your time to <code>timestamp</code> or you will get a wrong
          countdown.</p>
      </td>
      <td style="text-align:left"><code>Number</code>
      </td>
    </tr>
    <tr>
      <td style="text-align:left">largeImageKey</td>
      <td style="text-align:left">Defines the logo for the presence.</td>
      <td style="text-align:left"><code>String</code>
      </td>
    </tr>
    <tr>
      <td style="text-align:left">smallImageKey</td>
      <td style="text-align:left">Defines the small icon next to presence&apos;s logo.</td>
      <td style="text-align:left"><code>String</code>
      </td>
    </tr>
    <tr>
      <td style="text-align:left">smallImageText</td>
      <td style="text-align:left">Defines the text that will be shown to user when he will hover the small
        icon.</td>
      <td style="text-align:left"><code>String</code>
      </td>
    </tr>
  </tbody>
</table>```typescript
var presenceData: presenceData = {
    details: "My title",
    state: "My description",
    largeImageKey: "service_logo",
    smallImageKey: "small_service_icon",
    smallImageText: "You hovered me, and what now?",
    startTimestamp: 1564444631188,
    endTimestamp: 1564444634734
};
```

## Events

Events allow you to detect and handle some changes or calls that were made. You can subscribe to events using the `on` method.

```typescript
presence.on("UpdateData", async () => {
    // Do something when data gets updated.
});
```

There are few events available:

#### `UpdateData`

This event is fired every time presence is being updated.

#### `MediaKeys`

Fired when user uses media keys on his keyboard, [click here](presence-class.md#mediakeys) to get more information about media keys.

#### `iFrameData`

Fired when data is received from iFrame script.

