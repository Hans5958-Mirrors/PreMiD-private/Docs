# Coding

## Structure

You can choose if you want to code your Presence with [JavaScript ](https://www.javascript.com/)or [TypeScript](https://www.typescriptlang.org/).  
[TypeScript ](https://www.typescriptlang.org/)has some extra spicy type definitions so fixing bugs and identifying bugs is way easier.  
[Skip ](https://docs.premid.app/presence-devlopement/coding#some-useful-things-before-we-overthrow-this-beast)the [TypeScript](https://www.typescriptlang.org/) part if you only want to code with [JavaScript](https://www.javascript.com/).

### Creating the basics

1. Create a folder with the **name** \(not an URL\) ****of the service you want to support
2. Create a `presence.js` and a `metadata.json`

Almost all websites are using [_iframes_ ](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/iframe)\([Inlineframes](https://en.wikipedia.org/wiki/HTML_element#Frames)\).  
These html tags can contain multiple sources such as videos.  
But they're not relevant every time. Some are hidden or just not actively used.  
Check if you can extract, the information you need, without them before you do unnecessary work.

1. Check for them by browser console \(be sure that you're on the "Elements" tab\)
2. Search \(Strg+F or CMD+F\)
3. Type `document.querySelectorAll("iframe")`

{% code-tabs %}
{% code-tabs-item title="Output: iframe = true" %}
```javascript
NodeList [iframe]
 0: iframe
 length: 1
 __proto__: NodeList
```
{% endcode-tabs-item %}

{% code-tabs-item title="Output: iframe = false" %}
```javascript
NodeList []
 length: 0
 __proto__: NodeList
```
{% endcode-tabs-item %}
{% endcode-tabs %}

You have to create an `iframe.js` if your service is using them and you need e.g. a specific `<video>` tag from an [_iframe_](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/iframe). Don't forget to add that tag to your `metadata.json`.  
The code from your `iframe.js` is getting injected into every [_iframe_ ](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/iframe)onto the service so you can reach out for that metadata.

### Loading the Presence

1. Open the popup and hold the `Shift` button on your keyboard
2. "**Load Presence**" will appear in the Presences section
3. Click on it while you're still holding the `Shift` button
4. Choose your folder

## TypeScript introduction

Check [here ](../../contributing/introduction.md#installing-the-requirements)for a quick tutorial how to install [TypeScript](https://www.typescriptlang.org/).

### Compiling

We'll use [IntelliSense ](https://code.visualstudio.com/docs/editor/intellisense)and [TypeScript ](https://www.typescriptlang.org/)options to make our coding a little easier.  
Press Strg/CMD+Space bar to open it manually.  
  
For this feature you need a [TypeScript config file](https://www.typescriptlang.org/docs/handbook/tsconfig-json.html). Here's an example:

{% page-ref page="tsconfig.json.md" %}

Open a console in your folder and type `tsc -w` to compile presence into `/dist` folder.

## Some helpful things

### Hot-reloading

The website you're developing on is automatically reloading every time you save a file in your folder.

### Debugging

* We've created a `background.js` just for you. It shows you really useful information. You can open it from your extensions section in your browser.
* You can put`console.log("Test")` between your code and see if your browser console gives you that output. If yes then go on and try again after the next function. If not then there's an error above.
* If that doesn't help you either then ask a developer or contributor on our [Discord server](https://discord.premid.app) for help.

## Inside `presence.ts`

This is our main file that will be compiled to `presence.js` every time we change it. It also provides the [`Presence`](presence-class.md) class that will be used to create our presence.

{% page-ref page="presence-class.md" %}

## Placeholder

## Uploading your dream

Just open a Pull Request on this [GitHub repository](https://github.com/PreMiD/Presences). We'll take care of your baby, don't worry.

### Getting approved

Our main goal is to work **with** users so try to be as much friendly, respectful and informative as possible.  
Just ping a Developer in our Discord server to get that review on GitHub, or not. Depends on the code quality, appearance of assets and regular functionality.



