# Home

## Table of contents

* Contributing
  * [Introduction](contributing/introduction.md)
  * [Website](contributing/website.md)
* API
  * [Introduction](api/introduction.md)
  * [v1](api/v1/)
    * [Beta Access](api/v1/beta-access.md)
    * [Credits](api/v1/credits.md)
    * [Language File](api/v1/language-file-1.md)
    * [Presences](api/v1/presences.md)
    * [Users](api/v1/users.md)
  * [v2](api/v2/)
    * [Credits](api/v2/credits.md)
    * [Language File](api/v2/language-file.md)
    * [Versions](api/v2/version.md)
  * [Ping](api/ping.md)
* Presence development
  * [Introduction](presence-development/introduction.md)
  * [Coding](presence-development/coding/)
    * [metadata.json](presence-development/coding/metadata.json.md)
    * [tsconfig.json](presence-development/coding/tsconfig.json.md)

